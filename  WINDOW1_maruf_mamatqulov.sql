WITH SalesAggregated AS (
    SELECT
        s.cust_id,
        EXTRACT(YEAR FROM s.time_id) AS sale_year,
        s.channel_id,
        SUM(s.amount_sold) AS total_sales
    FROM
        sh.sales s
    WHERE
        EXTRACT(YEAR FROM s.time_id) IN (1998, 1999, 2001)
    GROUP BY
        s.cust_id,
        sale_year,
        s.channel_id
),
RankedSales AS (
    SELECT
        c.cust_first_name,
        c.cust_last_name,
        ch.channel_desc,
        sa.sale_year,
        sa.total_sales,
        RANK() OVER (PARTITION BY sa.channel_id, sa.sale_year ORDER BY sa.total_sales DESC) AS sales_rank
    FROM
        SalesAggregated sa
    JOIN sh.customers c ON sa.cust_id = c.cust_id
    JOIN sh.channels ch ON sa.channel_id = ch.channel_id
)
SELECT
    rs.cust_first_name,
    rs.cust_last_name,
    rs.channel_desc,
    rs.sale_year,
    TO_CHAR(rs.total_sales, 'FM999,999,999.00') AS total_sales_formatted,
    rs.sales_rank
FROM
    RankedSales rs
WHERE
    rs.sales_rank <= 300
ORDER BY
    rs.channel_desc,
    rs.sale_year,
    rs.sales_rank;
